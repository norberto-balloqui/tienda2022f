FROM ubuntu:18.04
RUN export DEBIAN_FRONTEND=noninteractive && apt update && apt-get -y install apache2 php7.2 php7.2-mysql libapache2-mod-php7.2
COPY tienda2022/ /var/www/html/tienda2022/
EXPOSE 80
ENTRYPOINT ["/usr/sbin/apache2ctl"]
CMD ["-D","FOREGROUND"]